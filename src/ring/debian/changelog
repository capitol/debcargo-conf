rust-ring (0.17.8-1) unstable; urgency=medium

  * Team upload.
  * Package ring 0.17.8 from crates.io using debcargo 2.6.1 (Closes: #1067602)

 -- Peter Michael Green <plugwash@debian.org>  Tue, 13 Aug 2024 17:09:38 +0000

rust-ring (0.17.5-1) unstable; urgency=medium

  * Team upload.
  * Package ring 0.17.5 from crates.io using debcargo 2.6.0
  * Update patches for new upsteam.
  * Upload to unstable.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 07 Nov 2023 18:17:58 +0000

rust-ring (0.17.3-1) experimental; urgency=medium

  * Team upload.
  * Package ring 0.17.3 from crates.io using debcargo 2.6.0
  * Add patch to disable slow tests.
  * Drop update-spin-to-0.9.patch, no longer needed.
  * Update patches for new upstream.
  * Remove windows-specific dependency that is not in Debian.
  * Remove dependency on js feature of getrandom, whic is not
    provided by Debian's rust-getrandom package.

 -- Peter Michael Green <plugwash@debian.org>  Sat, 14 Oct 2023 17:35:11 +0000

rust-ring (0.16.20-2) unstable; urgency=medium

  * Team upload.
  * Package ring 0.16.20 from crates.io using debcargo 2.5.0
  * Upgrade spin dependency to 0.9

 -- Alexander Kjäll <alexander.kjall@gmail.com>  Mon, 24 Oct 2022 20:11:21 -0400

rust-ring (0.16.20-1) unstable; urgency=medium

  * Team upload.
  * Package ring 0.16.20 from crates.io using debcargo 2.5.0 (Closes: #1010212)
  * Drop relax-deps.patch, no longer needed
  * Drop use-array-iter.patch, included in new upstream
  * Remove dev-dependency on wasm-bindgen-test, it's only used when testing
    with the wasm-bindgen target which we don't do in Debian.
  * Disable a test which depends on test data that is not in Debian.
  * Fix running tests with no-default-features.
  * Use collapse_features = true

 -- Peter Michael Green <plugwash@debian.org>  Thu, 28 Apr 2022 21:08:04 +0000

rust-ring (0.16.9-4) unstable; urgency=medium

  * Package ring 0.16.9 from crates.io using debcargo 2.4.2
  * use-array-iter.patch: Fix usage of array::into_iter
    Thanks to Logan Rosen (Closes: #961387)

 -- Sylvestre Ledru <sylvestre@debian.org>  Sat, 23 May 2020 23:38:56 +0200

rust-ring (0.16.9-3) unstable; urgency=medium

  * Team upload.
  * Package ring 0.16.9 from crates.io using debcargo 2.4.2

 -- Sylvestre Ledru <sylvestre@debian.org>  Mon, 20 Apr 2020 14:16:05 +0200

rust-ring (0.16.9-2) unstable; urgency=medium

  * Team upload.
  * Package ring 0.16.9 from crates.io using debcargo 2.2.10
  * librust-ring+lazy-static-dev is NEW.

 -- Sylvestre Ledru <sylvestre@debian.org>  Tue, 29 Oct 2019 09:07:09 +0100

rust-ring (0.16.9-1) unstable; urgency=medium

  * Package ring 0.16.9 from crates.io using debcargo 2.4.0 (Closes: #935673)

 -- kpcyrd <git@rxv.cc>  Tue, 22 Oct 2019 18:02:25 +0200

rust-ring (0.14.6-1) unstable; urgency=medium

  * Package ring 0.14.6 from crates.io using debcargo 2.2.10

 -- kpcyrd <git@rxv.cc>  Thu, 20 Jun 2019 17:05:16 -0700
