Author: Henning Holm <git@henningholm.de>
Origin: https://github.com/davidhewitt/pythonize/pull/66

Index: pythonize/CHANGELOG.md
===================================================================
--- pythonize.orig/CHANGELOG.md
+++ pythonize/CHANGELOG.md
@@ -1,3 +1,10 @@
+## Unreleased
+
+- Update to PyO3 0.22
+- Remove deprecated `depythonize`, use `depythonize_bound` instead
+- Remove deprecated `from_object`, use `from_object_bound` instead
+- Remove conversion from `PyDowncastError` to `PythonizeError`
+
 ## 0.21.1 - 2024-04-02
 
 - Fix compile error when using PyO3 `abi3` feature targeting a minimum version below 3.10
Index: pythonize/src/de.rs
===================================================================
--- pythonize.orig/src/de.rs
+++ pythonize/src/de.rs
@@ -1,23 +1,10 @@
-use pyo3::{types::*, Bound, PyNativeType};
+use pyo3::{types::*, Bound};
 use serde::de::{self, IntoDeserializer};
 use serde::Deserialize;
 
 use crate::error::{PythonizeError, Result};
 
 /// Attempt to convert a Python object to an instance of `T`
-#[deprecated(
-    since = "0.21.0",
-    note = "will be replaced by `depythonize_bound` in a future release"
-)]
-pub fn depythonize<'de, T>(obj: &'de PyAny) -> Result<T>
-where
-    T: Deserialize<'de>,
-{
-    let mut depythonizer = Depythonizer::from_object_bound(obj.as_borrowed().to_owned());
-    T::deserialize(&mut depythonizer)
-}
-
-/// Attempt to convert a Python object to an instance of `T`
 pub fn depythonize_bound<'py, T>(obj: Bound<'py, PyAny>) -> Result<T>
 where
     T: for<'a> Deserialize<'a>,
@@ -33,15 +20,6 @@ pub struct Depythonizer<'py> {
 
 impl<'py> Depythonizer<'py> {
     /// Create a deserializer from a Python object
-    #[deprecated(
-        since = "0.21.0",
-        note = "will be replaced by `Depythonizer::from_object_bound` in a future version"
-    )]
-    pub fn from_object(input: &'py PyAny) -> Self {
-        Self::from_object_bound(input.as_borrowed().to_owned())
-    }
-
-    /// Create a deserializer from a Python object
     pub fn from_object_bound(input: Bound<'py, PyAny>) -> Self {
         Depythonizer { input }
     }
Index: pythonize/src/error.rs
===================================================================
--- pythonize.orig/src/error.rs
+++ pythonize/src/error.rs
@@ -1,5 +1,5 @@
+use pyo3::PyErr;
 use pyo3::{exceptions::*, DowncastError, DowncastIntoError};
-use pyo3::{PyDowncastError, PyErr};
 use serde::{de, ser};
 use std::error;
 use std::fmt::{self, Debug, Display};
@@ -144,15 +144,6 @@ impl From<PyErr> for PythonizeError {
         }
     }
 }
-
-/// Handle errors that occur when attempting to use `PyAny::cast_as`
-impl<'a> From<PyDowncastError<'a>> for PythonizeError {
-    fn from(other: PyDowncastError) -> Self {
-        Self {
-            inner: Box::new(ErrorImpl::UnexpectedType(other.to_string())),
-        }
-    }
-}
 
 /// Handle errors that occur when attempting to use `PyAny::cast_as`
 impl<'a, 'py> From<DowncastError<'a, 'py>> for PythonizeError {
Index: pythonize/src/lib.rs
===================================================================
--- pythonize.orig/src/lib.rs
+++ pythonize/src/lib.rs
@@ -9,8 +9,8 @@
 //! # Examples
 //! ```
 //! use serde::{Serialize, Deserialize};
-//! use pyo3::Python;
-//! use pythonize::{depythonize, pythonize};
+//! use pyo3::{types::PyAnyMethods, Python};
+//! use pythonize::{depythonize_bound, pythonize};
 //!
 //! #[derive(Debug, Serialize, Deserialize, PartialEq)]
 //! struct Sample {
@@ -27,10 +27,10 @@
 //!     // Rust -> Python
 //!     let obj = pythonize(py, &sample).unwrap();
 //!
-//!     assert_eq!("{'foo': 'Foo', 'bar': None}", &format!("{}", obj.as_ref(py).repr().unwrap()));
+//!     assert_eq!("{'foo': 'Foo', 'bar': None}", &format!("{}", obj.bind(py).repr().unwrap()));
 //!
 //!     // Python -> Rust
-//!     let new_sample: Sample = depythonize(obj.as_ref(py)).unwrap();
+//!     let new_sample: Sample = depythonize_bound(obj.into_bound(py)).unwrap();
 //!
 //!     assert_eq!(new_sample, sample);
 //! });
@@ -40,8 +40,6 @@ mod de;
 mod error;
 mod ser;
 
-#[allow(deprecated)]
-pub use crate::de::depythonize;
 pub use crate::de::{depythonize_bound, Depythonizer};
 pub use crate::error::{PythonizeError, Result};
 pub use crate::ser::{
Index: pythonize/Cargo.toml
===================================================================
--- pythonize.orig/Cargo.toml
+++ pythonize/Cargo.toml
@@ -23,7 +23,7 @@ license = "MIT"
 repository = "https://github.com/davidhewitt/pythonize"
 
 [dependencies.pyo3]
-version = "0.21.0"
+version = "0.22"
 default-features = false
 
 [dependencies.serde]
@@ -35,10 +35,11 @@ default-features = false
 version = "1.0.2"
 
 [dev-dependencies.pyo3]
-version = "0.21.1"
+version = "0.22"
 features = [
     "auto-initialize",
     "macros",
+    "py-clone",
 ]
 default-features = false
 
