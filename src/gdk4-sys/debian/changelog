rust-gdk4-sys (0.9.0-2) unstable; urgency=medium

  * Team upload.
  * Package gdk4-sys 0.9.0 from crates.io using debcargo 2.6.1
  * Release to unstable

 -- Jeremy Bícha <jbicha@ubuntu.com>  Mon, 26 Aug 2024 18:04:13 -0400

rust-gdk4-sys (0.9.0-1) experimental; urgency=medium

  * Package gdk4-sys 0.9.0 from crates.io using debcargo 2.6.1
  * Depend on gir-rust-code-generator 0.20

 -- Matthias Geiger <werdahias@debian.org>  Sun, 11 Aug 2024 18:56:34 +0200

rust-gdk4-sys (0.8.2-8) unstable; urgency=medium

  * Team upload.
  * Package gdk4-sys 0.8.2 from crates.io using debcargo 2.6.1
  * Add breaks on old versions of rust-gdk4 and rust-gtk4-sys.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 06 Aug 2024 03:00:14 +0000

rust-gdk4-sys (0.8.2-7) unstable; urgency=medium

  * Upload to unstable
  * Package gdk4-sys 0.8.2 from crates.io using debcargo 2.6.1

 -- Matthias Geiger <werdahias@debian.org>  Thu, 01 Aug 2024 11:51:53 +0200

rust-gdk4-sys (0.8.2-6) experimental; urgency=medium

  * Mark v4.16 feature as flaky again

 -- Matthias Geiger <werdahias@riseup.net>  Wed, 17 Jul 2024 14:35:44 +0200

rust-gdk4-sys (0.8.2-5) experimental; urgency=medium

  * Stop patching in dmabuf feature, regenerate code against GTK4 4.14
  * Depend on GTK4 4.14

 -- Matthias Geiger <werdahias@riseup.net>  Tue, 16 Jul 2024 20:34:17 +0200

rust-gdk4-sys (0.8.2-4) unstable; urgency=medium

  * Fix dmabuf patch 

 -- Matthias Geiger <werdahias@riseup.net>  Tue, 21 May 2024 21:26:31 +0200

rust-gdk4-sys (0.8.2-3) unstable; urgency=medium

  * Really patch in dmabuf code
  * Drop patch skipping cross-validation tests

 -- Matthias Geiger <werdahias@riseup.net>  Tue, 21 May 2024 18:33:34 +0200

rust-gdk4-sys (0.8.2-2) unstable; urgency=medium

  * Add patch to include missing dmabuf code
  * Package gdk4-sys 0.8.2 from crates.io using debcargo 2.6.1

 -- Matthias Geiger <werdahias@riseup.net>  Tue, 07 May 2024 14:56:10 +0200

rust-gdk4-sys (0.8.2-1) unstable; urgency=medium

  * Upload to unstable 
  * Depend on gir-rust-code-generator 0.19.1

 -- Matthias Geiger <werdahias@riseup.net>  Sat, 04 May 2024 21:12:03 +0200

rust-gdk4-sys (0.8.1-2) experimental; urgency=medium

  * Team upload
  * Package gdk4-sys 0.8.1 from crates.io using debcargo 2.6.1
  * Restore: Depends libgtk-4-dev

 -- Jeremy Bícha <jbicha@ubuntu.com>  Fri, 19 Apr 2024 20:40:27 -0400

rust-gdk4-sys (0.8.1-1) experimental; urgency=medium

  * Team upload
  * Package gdk4-sys 0.8.1 from crates.io using debcargo 2.6.1

 -- Jeremy Bícha <jbicha@ubuntu.com>  Wed, 17 Apr 2024 16:47:33 -0400

rust-gdk4-sys (0.8.0-3) experimental; urgency=medium

  * Fix dependency on libgtk-4-dev

 -- Matthias Geiger <werdahias@riseup.net>  Wed, 28 Feb 2024 19:57:22 +0100

rust-gdk4-sys (0.8.0-2) experimental; urgency=medium

  * Marked v4_14 feature as broken

 -- Matthias Geiger <werdahias@riseup.net>  Wed, 14 Feb 2024 01:34:53 +0100

rust-gdk4-sys (0.8.0-1) experimental; urgency=medium

  * Package gdk4-sys 0.8.0 from crates.io using debcargo 2.6.1
  * Updated copyright years
  * Properly depend on packages needed for code regeneration
  * Test-depend on libgtk-4-dev
  * Versioned dependency on git-rust-code-generator

 -- Matthias Geiger <werdahias@riseup.net>  Sun, 11 Feb 2024 17:56:44 +0100

rust-gdk4-sys (0.7.2-2) unstable; urgency=medium

  * Team upload
  * Package gdk4-sys 0.7.2 from crates.io using debcargo 2.6.0
  * Drop obsolete MRSV patch
  * Release to unstable

 -- Jeremy Bícha <jbicha@ubuntu.com>  Thu, 28 Sep 2023 15:31:15 -0400

rust-gdk4-sys (0.7.2-1) experimental; urgency=medium

  * Package gdk4-sys 0.7.2 from crates.io using debcargo 2.6.0
  * Included patch for MSRV downgrade
  * Regenerate source code with debian tools before build

 -- Matthias Geiger <werdahias@riseup.net>  Fri, 08 Sep 2023 20:38:02 +0200

rust-gdk4-sys (0.6.3-2) unstable; urgency=medium

  * Package gdk4-sys 0.6.3 from crates.io using debcargo 2.6.0
  * Include patch to skip cross-validate test

 -- Matthias Geiger <werdahias@riseup.net>  Sun, 27 Aug 2023 14:53:35 +0100

rust-gdk4-sys (0.6.3-1) unstable; urgency=medium

  * Package gdk4-sys 0.6.3 from crates.io using debcargo 2.6.0
  * Removed inactive uploader, added my new mail address
  * Enabled tests so autopkgtest can run

 -- Matthias Geiger <werdahias@riseup.net>  Wed, 02 Aug 2023 00:05:53 +0200

rust-gdk4-sys (0.5.5-2) unstable; urgency=medium

  * Package gdk4-sys 0.5.5 from crates.io using debcargo 2.6.0

 -- Matthias Geiger <matthias.geiger1024@tutanota.de>  Sun, 25 Jun 2023 20:44:39 +0200

rust-gdk4-sys (0.5.5-1) experimental; urgency=medium

  * Package gdk4-sys 0.5.5 from crates.io using debcargo 2.6.0
  * Added myself to Uploaders
  * Dropped obsolete patches
  * Dropped obsolete patches
  * Added myself to Uploaders

 -- Matthias Geiger <matthias.geiger1024@tutanota.de>  Sat, 20 May 2023 13:45:46 +0200

rust-gdk4-sys (0.3.1-2) unstable; urgency=medium

  * debian/patches: Update to system-deps 6

 -- Sebastian Ramacher <sramacher@debian.org>  Wed, 05 Oct 2022 09:37:53 +0200

rust-gdk4-sys (0.3.1-1) unstable; urgency=medium

  * Package gdk4-sys 0.3.1 from crates.io using debcargo 2.4.4

 -- Henry-Nicolas Tourneur <debian@nilux.be>  Sun, 06 Feb 2022 16:47:06 +0100
