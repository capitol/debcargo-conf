rust-curl (0.4.44-4) unstable; urgency=medium

  * Team upload.
  * Package curl 0.4.44 from crates.io using debcargo 2.6.0
  * Bump socket2 dependency.
  * Disable "post" tests, they are overly specific and currently fail in Debian.
    I have confirmed this is not related to the socket2 bump and I suspect it
    is due to the recent update of libcurl from 8.3 to 8.4.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 31 Oct 2023 03:28:16 +0000

rust-curl (0.4.44-3) unstable; urgency=medium

  * Team upload.
  * Package curl 0.4.44 from crates.io using debcargo 2.6.0
  * Fix error in patch disabling upload_lots.

 -- Peter Michael Green <plugwash@debian.org>  Wed, 11 Oct 2023 04:00:01 +0000

rust-curl (0.4.44-2) unstable; urgency=medium

  * Team upload.
  * Package curl 0.4.44 from crates.io using debcargo 2.6.0
  * Disable upload_lots test, it's flaky.

 -- Peter Michael Green <plugwash@debian.org>  Wed, 11 Oct 2023 03:49:19 +0000

rust-curl (0.4.44-1) unstable; urgency=medium

  * Team upload.
  * Package curl 0.4.44 from crates.io using debcargo 2.5.0

 -- Fabian Gruenbichler <debian@fabian.gruenbichler.email>  Thu, 20 Oct 2022 20:48:34 -0400

rust-curl (0.4.43-1) experimental; urgency=medium

  * Team upload.
  * Package curl 0.4.43 from crates.io using debcargo 2.5.0

 -- Fabian Grünbichler <f.gruenbichler@proxmox.com>  Tue, 27 Sep 2022 20:56:29 -0400

rust-curl (0.4.39-1) unstable; urgency=medium

  * Team upload.
  * Package curl 0.4.39 from crates.io using debcargo 2.4.4

 -- Ximin Luo <infinity0@debian.org>  Sat, 23 Oct 2021 19:03:08 +0100

rust-curl (0.4.33-1) unstable; urgency=medium

  * Package curl 0.4.33 from crates.io using debcargo 2.4.3
  * Disable zlib-ng-compat, protocol-ftp and spnego features, the
    ftpmasters are currently rejecting new rust feature packages.
  * Disable static-with-ftp-disabled test, debian uses
    system libcurl even when linking statically and the
    system libcurl always has ftp enabled.

  [ Ximin Luo ]
  * Team upload.
  * Package curl 0.4.33 from crates.io using debcargo 2.4.3

 -- Peter Michael Green <plugwash@debian.org>  Mon, 07 Dec 2020 05:14:00 +0000

rust-curl (0.4.25-2) unstable; urgency=medium

  * Team upload.
  * Package curl 0.4.25 from crates.io using debcargo 2.4.2
  * hopefully the new debcargo fixes the autopkgtest issues.
    (hopefully closes 946733)

 -- Peter Michael Green <plugwash@debian.org>  Tue, 14 Apr 2020 09:13:55 +0000

rust-curl (0.4.25-1) unstable; urgency=medium

  * Team upload.
  * Package curl 0.4.25 from crates.io using debcargo 2.4.0

 -- Ximin Luo <infinity0@debian.org>  Thu, 28 Nov 2019 01:34:35 +0000

rust-curl (0.4.22-2) unstable; urgency=medium

  * Team upload.
  * Package curl 0.4.22 from crates.io using debcargo 2.2.10
  * Source upload for migration

 -- Sylvestre Ledru <sylvestre@debian.org>  Tue, 06 Aug 2019 18:20:06 +0200

rust-curl (0.4.22-1) unstable; urgency=medium

  * Team upload.
  * Package curl 0.4.22 from crates.io using debcargo 2.2.10

 -- Ximin Luo <infinity0@debian.org>  Thu, 30 May 2019 22:07:23 -0700

rust-curl (0.4.17-1) unstable; urgency=medium

  * Package curl 0.4.17 from crates.io using debcargo 2.2.7

 -- kpcyrd <git@rxv.cc>  Mon, 24 Sep 2018 22:23:41 +0100
