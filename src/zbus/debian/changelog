rust-zbus (4.3.1-5) unstable; urgency=medium

  * Team upload.
  * Package zbus 4.3.1 from crates.io using debcargo 2.6.1
  * Specify endian in test_raw. Otherwise on big endian architectures, the
    test tries to construct a message with a big endian header but a little
    endian body.

 -- Peter Michael Green <plugwash@debian.org>  Sat, 20 Jul 2024 07:49:30 +0000

rust-zbus (4.3.1-4) unstable; urgency=medium

  * Team upload.
  * Package zbus 4.3.1 from crates.io using debcargo 2.6.1
  * Stop patching async-io dependency.
  * Ignore tests that fail with file-not-found error.
  * Reinstate tokio-vsock 0.5 patch and extend/update it for new upstream.
  * Disable tests which require zbus_xml crate which is not in Debian.
  * Make an example build when tokio is enabled and async-io is not.
  * Stop marking all tests as broken, reestablish previous baseline.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 16 Jul 2024 23:31:07 +0000

rust-zbus (4.3.1-3) unstable; urgency=medium

  * Team upload.
  * Temporarily mark all tests as flaky; zbus is missing feature guards for
    tests to run (reported upstream) 

 -- Matthias Geiger <werdahias@riseup.net>  Tue, 16 Jul 2024 17:11:28 +0200

rust-zbus (4.3.1-2) unstable; urgency=medium

  * Team upload.
  * Upload to unstable 

 -- Matthias Geiger <werdahias@riseup.net>  Sun, 14 Jul 2024 15:49:49 +0200

rust-zbus (4.3.1-1) experimental; urgency=medium

  * Team upload.
  * Package zbus 4.3.1 from crates.io using debcargo 2.6.1
  * Drop obsoleted patches
  * Add patch to relax dependencies
  * Add patch to remove windows-only dependencies
  [Blair Noctis]
  * Downgrade nix to 0.27

 -- Matthias Geiger <werdahias@riseup.net>  Thu, 04 Jul 2024 21:32:03 +0200

rust-zbus (3.14.1-8) unstable; urgency=medium

  * Team upload.
  * Package zbus 3.14.1 from crates.io using debcargo 2.6.1
  * Bump async-fs dependency (Closes: #1074107)

 -- Peter Michael Green <plugwash@debian.org>  Mon, 24 Jun 2024 03:55:05 +0000

rust-zbus (3.14.1-7) unstable; urgency=medium

  * Team upload.
  * Package zbus 3.14.1 from crates.io using debcargo 2.6.1
  * Relax quick-xml dependency.

 -- Peter Michael Green <plugwash@debian.org>  Mon, 27 May 2024 07:07:56 +0000

rust-zbus (3.14.1-6) unstable; urgency=medium

  * Team upload.
  * Package zbus 3.14.1 from crates.io using debcargo 2.6.1
  * Upload to unstable.

 -- Peter Michael Green <plugwash@debian.org>  Thu, 02 May 2024 14:21:45 +0000

rust-zbus (3.14.1-5) experimental; urgency=medium

  * Team upload.
  * Package zbus 3.14.1 from crates.io using debcargo 2.6.1
  * Add patch for nix 0.27
  * Bump vsock to 0.4
  * Add patch for tokio-vsock 0.5.

 -- Peter Michael Green <plugwash@debian.org>  Sat, 24 Feb 2024 12:39:02 +0000

rust-zbus (3.14.1-4) unstable; urgency=medium

  * Team upload.
  * Package zbus 3.14.1 from crates.io using debcargo 2.6.1
  * Add patch for async-io 2.

 -- Peter Michael Green <plugwash@debian.org>  Thu, 08 Feb 2024 19:03:36 +0000

rust-zbus (3.14.1-3) unstable; urgency=medium

  * Team upload.
  * Amended remove-test-log.diff to skip test failing on s390x 

 -- Matthias Geiger <werdahias@riseup.net>  Fri, 13 Oct 2023 14:34:50 +0200

rust-zbus (3.14.1-2) unstable; urgency=medium

  * Team upload.
  * Updated remove-test-log.diff and relax-deps.diff
  * Created skip-connection-test.diff to skip broken test
  * Remove obsolete disable-e2e.diff
  * Enabled tokio-vsock and vsock feature as the relevant packages are
    in debian

 -- Matthias Geiger <werdahias@riseup.net>  Fri, 13 Oct 2023 02:06:00 +0200

rust-zbus (3.14.1-1) unstable; urgency=medium

  * Team upload
  * Package zbus 3.14.1 from crates.io using debcargo 2.6.0

 -- Jeremy Bícha <jbicha@ubuntu.com>  Sun, 08 Oct 2023 09:12:42 -0400

rust-zbus (3.12.0-5) unstable; urgency=medium

  * Team upload.
  * Package zbus 3.12.0 from crates.io using debcargo 2.6.0
  * Re-enable gvariant feature (Closes: #1038825)
  * Reduce context in disable-tests-requiring-daemon.diff to ease future update
    to new upstream (avoiding new upstream right now because of dependencies).

 -- Peter Michael Green <plugwash@debian.org>  Tue, 27 Jun 2023 22:17:36 +0000

rust-zbus (3.12.0-4) unstable; urgency=medium

  * Team upload.
  * Package zbus 3.12.0 from crates.io using debcargo 2.6.0
  * Upload to unstable.
  * Disable tests that depend on a running dbus daemon.
  * Clean up code that relies on test-log, the dependency was
    already patched away.
  * Disable tests that fail in the autopkgtest environment, despite
    passing in manual testing.
  * Establish baseline for autopkgtests.

 -- Peter Michael Green <plugwash@debian.org>  Tue, 27 Jun 2023 03:47:47 +0000

rust-zbus (3.12.0-3) experimental; urgency=medium

  * Team upload.
  * Package zbus 3.12.0 from crates.io using debcargo 2.6.0
  * Amend patch to relax dependency on quick-xml (Closes: #1038756)

 -- Matthias Geiger <matthias.geiger1024@tutanota.de>  Wed, 21 Jun 2023 20:35:12 +0200

rust-zbus (3.12.0-2) experimental; urgency=medium

  * Team upload.
  * Package zbus 3.12.0 from crates.io using debcargo 2.6.0
  * Rebase patch tom make package installable

 -- Matthias Geiger <matthias.geiger1024@tutanota.de>  Tue, 20 Jun 2023 17:58:12 +0200

rust-zbus (3.12.0-1) experimental; urgency=medium

  * Team upload.
  * Package zbus 3.12.0 from crates.io using debcargo 2.6.0
  * Dropped obsolete patches, relaxed dependencies

 -- Matthias Geiger <matthias.geiger1024@tutanota.de>  Sun, 28 May 2023 16:38:40 +0200

rust-zbus (1.9.2-4) unstable; urgency=medium

  * Team upload.
  * Backport 2a232177386ec0be0741149e0497e2dc516e7693 from upstream to
    publicly export zvariant

 -- Reinhard Tartler <siretart@tauware.de>  Sun, 01 Jan 2023 17:35:25 -0500

rust-zbus (1.9.2-3) unstable; urgency=medium

  * Team upload.
  * Package zbus 1.9.2 from crates.io using debcargo 2.5.0
  * Bump dependency on serde-xml-rs.

 -- Peter Michael Green <plugwash@debian.org>  Sun, 09 Oct 2022 11:39:17 +0000

rust-zbus (1.9.2-2) unstable; urgency=medium

  * Team upload.
  * Package zbus 1.9.2 from crates.io using debcargo 2.5.0
  * Add patch for nix 0.24 (Closes: 1012418)

 -- Peter Michael Green <plugwash@debian.org>  Sat, 11 Jun 2022 16:03:02 +0000

rust-zbus (1.9.2-1) unstable; urgency=medium

  * Package zbus 1.9.2 from crates.io using debcargo 2.5.0

  [ Henry-Nicolas Tourneur ]
  * Team upload.
  * Package zbus 1.9.2 from crates.io using debcargo 2.4.4

 -- Peter Michael Green <plugwash@debian.org>  Sat, 05 Feb 2022 13:41:40 +0000

rust-zbus (1.0.0-3) unstable; urgency=medium

  * Team upload.
  * Package zbus 1.0.0 from crates.io using debcargo 2.4.4
  * Set test_is_broken, the tests have never passed on i386 or ppc64el
    and are skipped in plain testing tests but run in unstable to
    testing migration tests, which is blocking migration of other
    packages (e.g. dh-cargo) to testing.
  * set collapse_features = true, the features are already collapsed
    in the packages in the archive.

 -- Peter Michael Green <plugwash@debian.org>  Wed, 01 Sep 2021 14:50:03 +0000

rust-zbus (1.0.0-2) unstable; urgency=high

  * Rebuild.

 -- Andrej Shadura <andrewsh@debian.org>  Sun, 10 Jan 2021 10:24:39 +0100

rust-zbus (1.0.0-1) unstable; urgency=medium

  * Package zbus 1.0.0 from crates.io using debcargo 2.4.3

 -- Andrej Shadura <andrewsh@debian.org>  Tue, 22 Dec 2020 15:26:58 +0100
